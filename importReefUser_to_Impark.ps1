###************* USER CREATION SCRIPT Impark AD ******* ########
#
# This script is intended to support Help Desk routine task of
# creation of users in Impark AD. Using this script can reduce time significantly
# also preventing human error when entering data multiple times. 
#
#
# This script imports information from users in REEF AD then uses that information to
# create a new user in IMPARK AD. if user doesn't exist in Reef AD then script won't work.
#
# Created by: Israel Acosta
# IT Consultant
# date: March 06, 2020
# Updated: MArch 10,2020 by: Israel Acosta

function user_exist($samAccountName){

try {

    if(Get-ADUser $samAccountName -ErrorAction stop){
    Write-Host "User already exist in AD, please check the username and run the script again"
    pause
    exit
    }
}

catch {

    Write-Host "Creating new User in Impark AD.."
}

}
cls
$reefuser = Read-Host "Enter username as Name.Lastname"
try{
    if(Get-ADUser $reefuser -server "reef" -ErrorAction stop){$importReef_User = Get-ADUser $reefuser -Server "reef" -Properties *} 
    }
catch {

    Write-Host "User doesn't exist, need to be created in REEF AD first."
    pause
    exit
    }
$firstletter=($importReef_User).givenname[0]
if ($importReef_User.Surname.Length -lt 5){
    $fiveLastname = $importReef_User.Surname
}else{
    $fiveLastname = ($importReef_User).surname.Substring(0,5)
}

$newImparkUser = $firstletter+$fiveLastname
$samaccountname = $newImparkUser.ToLower()+"01"
user_exist($samaccountname)


$OUpath = "OU=Impark,OU=PRODUCTION,DC=impark,DC=com"
$department = ($importReef_User).department
$password = 'Welcome1'
$company = "Impark"
$NewUserName= ($importReef_User).GivenName
$newUserLastName = ($importReef_User).Surname
$domain = "impark.com"
$username = ($importReef_User).Name
$UPN = $samaccountname+"@"+$domain ##<-- this can be 'emailaddress' field if required. 
$phonenumber = ($importReef_User).OfficePhone
$streetaddress = ($importReef_User).StreetAddress
$city = ($importReef_User).city
$province = ($importReef_User).state
$postal = ($importReef_User).postalcode
$title = ($importReef_User).Title
$country = ($importReef_User).country


$username
$email
$phonenumber
$streetaddress
$city
$province
$title
$country
$domain
$samaccountname

$confirmation = Read-Host "Ready to proceed? [y/n]"
while ($confirmation -ne "y")
{
    if ($confirmation -eq 'n'){exit}
    $confirmation = Read-Host "Ready to proceed? [y/n]"
}



New-ADUser -Name $username -GivenName $NewUserName -Surname $newUserLastName `
-DisplayName $username -SamAccountName $samAccountName `
-UserPrincipalName $samaccountname"@"$domain -Path $OUpath `
-AccountPassword (ConvertTo-SecureString $password -AsPlainText -Force)`
-ChangePasswordAtLogon $true -Enabled $true -Company $company -Title $title -Country $country `
-Department $department -officephone $phonenumber -StreetAddress $streetaddress -City $city -State $province -PostalCode $postal

Start-Sleep -Seconds 15

Get-ADUser $samAccountName -Properties name,City,Company,Department,DisplayName,DistinguishedName,EmailAddress,Memberof,OfficePhone,proxyAddresses,SamAccountName
