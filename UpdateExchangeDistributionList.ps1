###************* USER CREATION SCRIPT ******* ########
#
# This script is intended to support Help Desk when creating new users. 
# This Script is meant to be used to connect to Exchange and add users 
# from a text file to the correct Dsitribution List. 
# there is no user input in this script other than exchange login and usernames read from file.
#
# Created by: Israel Acosta
# IT Consultant
# date: March 06, 2020
# Updated: MArch 09,2020 by: Israel Acosta
cls
Connect-ExchangeOnlineShell -Credential Get-Credential
$date=Get-Date -Format yyyy-mm-dd

$path = "C:\powershell\Reef Parking_newusers_log.txt"
if(Test-Path $path){
    $users = get-content -Path $path

foreach ($user in $users){

    Add-DistributionGroupMember -Identity "Everyone (Reef Parking)" -Member $user
}
Move-Item -path "C:\powershell\Reef Parking_newusers_log.txt" -destination "C:\powershell\Created_users\$date _Reef Parking_newusers_log.txt"
}

$path = "C:\powershell\Reef Technology_newusers_log.txt"

if(Test-Path $path){
$users = get-content -Path $path

foreach ($user in $users){

    Add-DistributionGroupMember -Identity "Everyone (Reef Technology)" -Member $user 
}
move-Item -path "C:\powershell\Reef Technology_newusers_log.txt" -destination "C:\powershell\Created_users\$date _Reef Technology_newusers_log.txt"
}

$path = "C:\powershell\Reef Kitchens_newusers_log.txt"
if( Test-Path $path){
$users = get-content -Path $path

foreach ($user in $users){

    Add-DistributionGroupMember -Identity "Everyone (Reef Kitchens)" -Member $user
}
Move-Item -path "C:\powershell\Reef Kitchens_newusers_log.txt" -destination "C:\powershell\Created_users\$date _Reef Kitchens_newusers_log.txt"
}




#$id = Get-PSSession | select id
Disconnect-ExchangeOnlineShell #-SessionID $id