###************* USER CREATION SCRIPT ******* ########
#
# This script is intended to support Help Desk routine task of
# creation of users. Using this script can reduce time significantly
# also preventing human error when entering data multiple times. 
#
# Created by: Israel Acosta
# IT Consultant
# date: March 06, 2020
# Updated: MArch 06,2020 by: Israel Acosta

#************ FUNCTIONS BLOCK ************************

function get_Start-Sleep($seconds) {
    $doneDT = (Get-Date).AddSeconds($seconds)
    while($doneDT -gt (Get-Date)) {
        $secondsLeft = $doneDT.Subtract((Get-Date)).TotalSeconds
        $percent = ($seconds - $secondsLeft) / $seconds * 100
        Write-Progress -Activity "Executing script ...." -Status "Please Wait..." -SecondsRemaining $secondsLeft -PercentComplete $percent
        [System.Threading.Thread]::Sleep(500)
    }
    Write-Progress -Activity "Executing script ...." -Status "Please Wait..." -SecondsRemaining 0 -Completed
}

function get_domain() {
cls
    write-host "1) reefparking.com"
    write-host "2) reeftechnology.com"
    write-host "3) reefkitchens.com"

$OP = Read-Host "Enter domain option [1, 2, or 3]"

switch ($OP){
    1 {$OP = "reefparking.com";break}
    2 {$OP = "reeftechnology.com";break}
    3 {$OP = "reefkitchens.com";break}
    default {exit}
    }
    return $OP
}

function get_company() {
cls
    write-host "1) Reef Parking"
    write-host "2) Reef Technology"
    write-host "3) Reef Kitchens"
$OP = Read-Host "Enter Company option [1, 2, or 3]"

switch ($OP){
    1 {$OP = "Reef Parking";break}
    2 {$OP = "Reef Technology";break}
    3 {$OP = "Reef Kitchens";break}
    default {exit}
    }
    return $OP
}

#as users get created more locations can be added here
function get_department() {
cls
    write-host "`n1) Operations"
    write-host "`n2) Accounting"
    write-host "`n3) Launch and Enabling"
    write-host "`n4) Corporate"
    #write-host "1) Other Department"
$OP = Read-Host "`nEnter Deparment Option #"

switch ($OP){
    1 {$OP = "Operations";break}
    2 {$OP = "Accounting";break}
    3 {$OP = "Launch and Enabling";break}
    4 {$OP = "Corporate";break}
    #2 {$OP = "Other departmen";break} <-- use this format to add more options
    default {exit}
    }
    return $OP
}

function get_OrgUnit {
cls
Write-Host "Enter Location (Org Unit)"
$OUlist = Get-Content "C:\users\$env:username\Documents\powershell\Reef_OU_List.txt"
$i=0
foreach($option in $OUlist){Write-Host $i")" $OUlist[$i];$i++}

$OP = Read-Host "Enter Organizational Unit (New User Location) [1,2,3, ...,33]"

switch ($OP){
    $OP {$OP = $OUlist[$OP]}
    default {exit}
    }

    return $OP
}

#get_phoneNumber function. user should enter (copy/paste) phone number
# from SNOW request; function will return correct format
function get_phoneNumber() {
cls
$phonenum = Read-Host "Enter Phone Number"

$newphonenumber = "+1."+$phonenum
$newphonenumber = $newphonenumber -replace '-','.'
return $newphonenumber
}


function get_officeADGroup() {
cls
    write-host "1) SG-Office365-E1"
    write-host "2) SG-Office365-E2"
    write-host "3) SG-Office365-E3"
$OP = Read-Host "Enter Office Security Group option [1, 2, or 3]"

switch ($OP){
    1 {$OP = "SG-Office365-E1";break}
    2 {$OP = "SG-Office365-E2";break}
    3 {$OP = "SG-Office365-E3";break}
    default {exit}
    }
    return $OP
}


function get_OUPath ($OU) {

    if($OU -eq "Reef Kitchens"){
        $OUpath = "OU=ReefKitchens,OU=Users,OU=PRODUCTION,DC=reef,DC=reeftechnology,DC=com"
    }elseif($OU -eq "Reef Technology"){
        $OUpath = "OU=ReefTechnology,OU=Users,OU=PRODUCTION,DC=reef,DC=reeftechnology,DC=com"
    }else{
        $OUpath = "OU="+$OU+",OU=Impark,OU=Users,OU=PRODUCTION,DC=reef,DC=reeftechnology,DC=com"
    }

    return $OUpath
}


function user_exist($samAccountName){

if((get-aduser $samAccountName) -eq $null){

}else{

    Write-Host "User already exist in AD, please check the username and run the script again"
    pause
    exit
}

}

# *****************END OF FUNCTIONS BLOCK ****************************
cls
Write-Host "`n**************** REEF TECHNOLOGY *********************"
Write-Host "*                                                      *"
Write-Host "* This script was created for internal use ONLY        *"
Write-Host "* This program will create an Active Directory         *"
Write-Host "* User on REEF Domain                                  *" 
Write-Host "* Press CTRL+C to exit at any time                     *"
Write-Host "*                                                      *"
Write-Host "********************************************************"
pause 


cls
$NewUserName= Read-Host "Enter New User First Name"
cls
$newUserLastName = Read-Host "Last name"
$samAccountName = $NewUserName+"."+$newUserLastName
user_exist($samAccountName)

$department = get_department 
$password = 'Welcome1' 
$company = get_company


$domain = get_domain
$domain
$OU = get_OrgUnit
$OU
$username = $NewUserName +" "+ $newUserLastName


$OUpath = get_OUPath($OU)

$email = $samAccountName+"@"+$domain
$phonenumber = get_phoneNumber
$streetaddress = Read-Host "Street Address"
$city = Read-Host "City"
$province = Read-Host "State/Province"
$postal = Read-Host "Postal Code"
$title = Read-Host "Title"
$country = Read-Host "Country [US / CA]"
$securityGroup = "SG-MFA" 
$officeADGroup = get_OfficeADGroup

cls
$username
$email
$phonenumber
$streetaddress
$city
$province
$title
$officeADGroup
$OU
$OUpath

$confirmation = Read-Host "Ready to proceed? [y/n]"
while ($confirmation -ne "y")
{
    if ($confirmation -eq 'n'){exit}
    $confirmation = Read-Host "Ready to proceed? [y/n]"
}
# The next block will create the user in REEF AD with the parameters entered by IT Help Desk
New-ADUser -Name $username -GivenName $NewUserName -Surname $newUserLastName `
-DisplayName $username -SamAccountName $samAccountName `
-UserPrincipalName $NewUserName"."$newUserLastName"@"$domain -Path $OUpath `
-AccountPassword (ConvertTo-SecureString $password -AsPlainText -Force)`
-ChangePasswordAtLogon $true -Enabled $true -Company $company -EmailAddress $email -Title $title -Country $country `
-Department $department -officephone $phonenumber -StreetAddress $streetaddress -City $city -State $province -PostalCode $postal

Write-Host "Creating User ...."
get_Start-Sleep (15) 

$username = Get-ADUser $samAccountName
$username.proxyaddresses = "SMTP:"+$email
Set-ADUser -Instance $username

Add-ADGroupMember -Identity $securityGroup -Members $samAccountName
Add-ADGroupMember -Identity $officeADGroup -Members $samAccountName

Get-ADUser $samAccountName -Properties name,City,Company,Department,DisplayName,DistinguishedName,EmailAddress,Memberof,OfficePhone,proxyAddresses,SamAccountName

# create a folder and file to log users created. this file can be used to update Exchange delivery groups once
# mailbox creationg is completed.
$path = "C:\powershell\$company"+"_newusers_log.txt"
Add-Content $path $samAccountName

get_Start-Sleep (5)

Write-Host "`n"
Write-Host "***************Export user to Impark AD *********************"
Write-Host "Executing remote script on Impark AD"
Write-Host "Prese <CTRL+C> to cancel"
Write-Host "`n"
Write-Host "*************************************************************"

get_Start-Sleep (10)

# call to remote script on dc1dc2 (Impark Domain) to create new user on that domain. 
Invoke-Command -ComputerName dc1dc2 -Credential (Get-Credential) -ScriptBlock {C:\Powershell\importReefUser_to_Impark.ps1}